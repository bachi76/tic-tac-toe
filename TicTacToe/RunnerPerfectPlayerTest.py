from __future__ import print_function

from BoardGamesSimulator.Actors.DeepQLearningPlayer import DeepQLearningPlayer
from BoardGamesSimulator.Actors.HumanPlayer import HumanPlayer
from BoardGamesSimulator.Actors.PerfectTicTacToePlayer import PerfectTicTacToePlayer
from BoardGamesSimulator.Actors.QLearningPlayer import QLearningPlayer

from BoardGamesSimulator import Environment
from BoardGamesSimulator.TicTacToe.TicTacToeGame import TicTacToeGame

"""
The starting point for experiments. Define the players and run simulations.
"""


# print("Starting QLearningPlayer training...")
# playerQLearn1 = QLearningPlayer()
# playerQLearn2 = QLearningPlayer()
#
# for i in range(1, 51):
#     print("Batch {}".format(i))
#     # Start qLearner as Player B, since it'll play against the human Player A later
#     history = Simulator.run(playerQLearn1, playerQLearn2, 3000)[0]
#
#     # Show an example of a lost game.
#     for game in [x for x in history if x.state == x.WIN_PMINUS1][0:1]:
#         print(np.array(game.history))
#
#
# print("")
# print("Test against random player")
#
# for i in range(1, 51):
#     print("Batch {}".format(i))
#     history = Simulator.run(RandomPlayer(), playerQLearn1, 1000)[0]
#
#     # Show an example of a lost game.
#     for game in [x for x in history if x.state == x.WIN_PMINUS1][0:1]:
#         print(np.array(game.history))
#
#
# print("")
#

playerQLearn2 = QLearningPlayer()
playerDQL2 = DeepQLearningPlayer()
playerDQL2.model.load_weights("../storage/playerDQL.h5")

for i in range(50):
    print("Batch {}".format(i))
    history = Environment.run(PerfectTicTacToePlayer(), playerDQL2, TicTacToeGame, 1000)[0]

# serialize weights to HDF5
playerDQL2.model.save_weights("../storage/playerDQL.h5")
print("Saved model to disk")

''' Now you play '''
# Your turn to play...
print("")
print("---- Your turn, human! (5 games, keys 1-9, num block layout) ----")
Environment.run(HumanPlayer(), playerDQL2, TicTacToeGame, 5)