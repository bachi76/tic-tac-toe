from __future__ import print_function

import numpy as np

from BoardGamesSimulator import Environment
from BoardGamesSimulator.Actors.DNNPlayer import DNNPlayer
from BoardGamesSimulator.Actors.HumanPlayer import HumanPlayer
from BoardGamesSimulator.Actors.QLearningPlayer import QLearningPlayer
from BoardGamesSimulator.Actors.RandomPlayer import RandomPlayer
import matplotlib.pyplot as plt

from BoardGamesSimulator.TicTacToe.TicTacToeGame import TicTacToeGame

"""
The starting point for experiments. Define the players and run simulations.
"""


''' DNN Player '''

print("Gathering random player game data for training...")
history = Environment.run(RandomPlayer(), RandomPlayer(), TicTacToeGame, 20000)[0]
playerDnn = DNNPlayer()
playerDnn.train(history)

print("")

''' QLearningPlayer '''

print("Starting QLearningPlayer training...")
playerQLearn = QLearningPlayer()
winsRandom = []
for i in range(1, 301):
    print("Batch {}".format(i))
    # Start qLearner as Player B, since it'll play against the human Player A later
    history = Environment.run(RandomPlayer(), playerQLearn, 200)

    # Show an example of a lost game.
    for game in [x for x in history[0] if x.state == x.WIN_PMINUS1][0:1]:
        print(np.array(game.history[0]))

    winsRandom.append(history[1])

print("")

plt.plot(winsRandom)
plt.ylabel('Opponent wins')
plt.show()


# ''' The match! '''
# It seems this additional training is an example of overfitting:
# => The QLearner becomes weaker after the unbalanced training against the DNNPlayer

# print ("Starting DNNPlayer against QLearningPlayer evaluation...")
# history = Simulator.run(playerDnn, playerQLearn, 3000)[0]

# Show some example games. AIPlayer lacks all defensive training at the moment.
# for game in history[950:960]:
#    print(np.array(game.history))
#    print("")


''' Now you play '''
# Your turn to play...
print("")
print("---- Your turn, human! (5 games, keys 1-9, num block layout) ----")
Environment.run(HumanPlayer(), playerQLearn, TicTacToeGame, 5)