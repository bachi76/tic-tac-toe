from __future__ import print_function
import numpy as np
from skimage.util import view_as_windows

class TicTacToeGame:

    IN_PROGRESS = 0
    DRAW = 1
    WIN_P1 = 2
    WIN_PMINUS1 = 3


    def __init__(self):

        self.state = self.IN_PROGRESS
        self.last_player = 0
        self.last_move = -1

        # Store the game state and history (without compressing it)
        # Max. 9 moves = 9 x game state (3x3)

        self.board = np.zeros((3, 3))
        self.history = []


    def move(self, player, pos):

        if player not in [-1, 1]:
            raise ValueError("Player must be 1 or -1!")

        if self.state != self.IN_PROGRESS:
            raise OverflowError("Game already finished!")

        if player == self.last_player:
            raise ValueError("Not your turn!")

        if self.board.flat[pos] == 0:

            # Make the move and store board state in history
            self.board.flat[pos] = player
            self.history.append(self.board.copy())

            self.last_player = player
            self.last_move = pos
            self.update_state()

        else:
            raise ValueError("Field already occupied!")


    def update_state(self):

        # Do some np matrix magic to get an array of all sums of all board lines
        checks = np.concatenate([
            self.board.sum(0),
            self.board.sum(1),
            [self.board.trace()],
            [np.fliplr(self.board).trace()]
        ])

        # If we have 3 or -3 as sum of any line, it's a win

        if 3 in checks:
            self.state = self.WIN_P1

        elif -3 in checks:
            self.state = self.WIN_PMINUS1


        elif 0 in self.board.flat:
            self.state = self.IN_PROGRESS

        else:
            self.state = self.DRAW


    # Get all actions that are available in the current state
    def available_actions(self):

        if self.state == self.IN_PROGRESS:
            return np.where(self.board.flatten() == 0)[0]
        else:
            return np.array()


