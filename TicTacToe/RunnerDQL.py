from __future__ import print_function

from random import random

import matplotlib.pyplot as plt
import numpy as np

from BoardGamesSimulator import Environment
from BoardGamesSimulator.Actors.DeepQLearningPlayer import DeepQLearningPlayer
from BoardGamesSimulator.Actors.HumanPlayer import HumanPlayer
from BoardGamesSimulator.Actors.QLearningPlayer import QLearningPlayer
from BoardGamesSimulator.Actors.RandomPlayer import RandomPlayer
from BoardGamesSimulator.TicTacToe.TicTacToeGame import TicTacToeGame

"""
The starting point for experiments. Define the players and run simulations.
"""

''' Train a QLearningPlayer as sparring partner'''

print("Starting QLearningPlayer training...")
playerQLearn1 = QLearningPlayer()
playerQLearn2 = QLearningPlayer()

for i in range(10):
    print("Batch {}".format(i))
    history = Environment.run(playerQLearn1, playerQLearn2, TicTacToeGame, 1000)[0]



print("Starting DeepQLearningPlayer training...")
playerDQL1 = DeepQLearningPlayer()
playerDQL1.RANDOM_QUOTE_STOP = 0.5

playerDQL2 = DeepQLearningPlayer()
playerDQL2.debug = False
playerDQL2.model.load_weights("../storage/playerDQL.h5")
# playerDQL2.BACKPROP_STEPS = 0

totalBatches = 40
winsQLearner = np.zeros(totalBatches)
winsRandom = np.zeros(totalBatches)

for i in range(totalBatches):
    print("Batch {}".format(i))

    if random() < 0.5:
        print ("Playing against playerQLearn1...")
        history = Environment.run(playerQLearn1, playerDQL2, TicTacToeGame, 1000)
        winsQLearner[i] = history[1]
    else:
        print("Playing against RandomPlayer...")
        history = Environment.run(RandomPlayer(), playerDQL2, TicTacToeGame, 1000)
        winsRandom[i] = history[1]

    # Show an example of a lost game.
#    for game in [x for x in history if x.state == x.WIN_PMINUS1][0:1]:
#        print(np.array(game.history))

plt.plot(winsRandom, 'bo')
plt.plot(winsQLearner, 'ro')
plt.ylabel('Opponent wins')
plt.show()
# print("")

# ''' The match! '''
# It seems this additional training is an example of overfitting:
# => The QLearner becomes weaker after the unbalanced training against the DNNPlayer

# print ("Starting DNNPlayer against QLearningPlayer evaluation...")
# history = Simulator.run(playerDnn, playerQLearn, 3000)[0]

# Show some example games. AIPlayer lacks all defensive training at the moment.
# for game in history[950:960]:
#    print(np.array(game.history))
#    print("")


''' Now you play '''
# Your turn to play...
print("")
print("---- Your turn, human! (10 games, keys 1-9, num block layout) ----")
playerDQL2.batchLearnSize = 1
playerDQL2.debug = True

Environment.run(HumanPlayer(), playerDQL2, TicTacToeGame, 10)