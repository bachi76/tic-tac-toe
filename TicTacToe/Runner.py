from __future__ import print_function
from BoardGamesSimulator import Environment
from BoardGamesSimulator.Actors.DNNPlayer import DNNPlayer
from BoardGamesSimulator.Actors.RandomPlayer import RandomPlayer
import numpy as np

from BoardGamesSimulator.TicTacToe.TicTacToeGame import TicTacToeGame

"""
The starting point for experiments. Define the players and run simulations.
"""

print("Gathering random player game data for training...")
history = Environment.run(RandomPlayer(), RandomPlayer(), TicTacToeGame, 10000)[0]

print("")
print("Starting AIPlayer training...")
playerAI = DNNPlayer()
playerAI.train(history)

print("")
print ("Starting AIPlayer evaluation...")
Environment.run(playerAI, RandomPlayer(), TicTacToeGame, 1000)

print("")
print("A lost game:")

# Show an example of a lost game. AIPlayer lacks all defensive training at the moment.
for game in [x for x in history if x.state == x.WIN_PMINUS1][0:1]:
    print(np.array(game.history))

