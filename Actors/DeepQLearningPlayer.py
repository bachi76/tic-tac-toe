import hashlib
import random

from keras.layers import Dropout, Dense, BatchNormalization
from keras.models import Sequential
import numpy as np
from keras.utils import np_utils


class DeepQLearningPlayer:
    def __init__(self, inputSize=9):


        random.seed()

        self.RANDOM_QUOTE_START = 0.10
        self.RANDOM_QUOTE_STOP = 0.00
        self.RANDOM_QUOTE_DECREASE = 0.0005

        self.inputSize = inputSize
        self.BACKPROP_STEPS = 5

        self.batchLearnSize = 500
        self.batchVsMemoryFactor = 4
        self.updateCounter = 0
        self.memoryX = []
        self.memoryY = []
        self.memoryZ = []

        self.val = None
        self.memory = {}
        self.stateHistory = []

        self.randomQuote = self.RANDOM_QUOTE_START
        self.debug = False

        # create the ANN that should predict the q value
        self.model = Sequential()
        self.model.add(Dense(12, input_dim=inputSize, init='uniform', activation='relu'))
        self.model.add(Dropout(0.1))
        self.model.add(Dense(24, init='uniform', activation='relu'))
        self.model.add(Dropout(0.1))
        self.model.add(Dense(24, init='uniform', activation='relu'))
        self.model.add(Dropout(0.1))
        self.model.add(Dense(12, init='uniform', activation='relu'))
        self.model.add(Dense(1, init='uniform', activation='tanh'))

        # Compile model
        self.model.compile(loss='mean_squared_error', optimizer='adam')


    def move(self, game):

        # For all possible moves, try to find a q-value entry in our memory
        # Select the best move. If we have no entry, choose a random move.

        bestMove = random.choice(game.available_actions())
        bestQValue = 0
        
        if random.random() > self.randomQuote:
            
            for action in game.available_actions():
                examinedState = game.board.copy().flatten()
                examinedState[action] = self.val

                predQValue = self.model.predict(self.flatStateToANNInput(examinedState))

                if self.debug:
                    print("Move {} - QValue: {}".format(action, predQValue))

                if predQValue > bestQValue:
                    bestQValue = predQValue
                    bestMove = action

        self.randomQuote = max(self.randomQuote - self.RANDOM_QUOTE_DECREASE, self.RANDOM_QUOTE_STOP)

        return bestMove

    # Called by the game engine if the last action leads to a reward
    def reward(self, game, reward):

        # Propagate the discounted reward backward in the memory
        if game.last_player == self.val:
            idx = -1
        else:
            idx = -2

        for i in range(0, self.BACKPROP_STEPS):

            try:
                state = game.history[idx].flatten()
                self.updateQValue(state, reward)
                reward /= 2.0
                idx -= 2
            except IndexError:
                pass


    def updateQValue(self, state, value):

        # Scale the reward (-1 | 0 | 1) to (0 | 0.5 | 1)
        value = value / 2 + 0.5

        X = self.flatStateToANNInput(state)
        y = np.array([value])

        # Calculating the TD Error
        z = abs(self.model.predict(X) - value)


        self.memoryX.append(X[0])
        self.memoryY.append(y)
        self.memoryZ.append(z)

        self.updateCounter += 1

        if self.updateCounter >= self.batchVsMemoryFactor * self.batchLearnSize:

            self.updateCounter = 0
            batchX = []
            batchY = []

            for i in range(0, self.batchLearnSize):

                # Select the one with the highest TD range from a random sample
                pos = random.randrange(len(self.memoryX))
                for j in range(0, 4):
                    candidate = random.randrange(len(self.memoryX))
                    z_can = self.memoryZ[candidate]
                    z_best = self.memoryZ[pos]
                    # print ("{} vs {}".format(z_can, z_best))

                    if z_can > z_best:
                        pos = candidate

                batchX.append(self.memoryX.pop(pos))
                batchY.append(self.memoryY.pop(pos))
                self.memoryZ.pop(pos)

            if self.debug:
                print("Fitting on this data:")
                print (np.array(batchX))
                print (np.array(batchY))

            self.model.fit(np.array(batchX), np.array(batchY), verbose=0, nb_epoch=30, batch_size=10)



    # def hash(self, state):
    #     # Could probably be optimized
    #     # maybe hash(str(state))?
    #     return hashlib.sha1(state).hexdigest()

    def flatStateToANNInput(self, state):
        state = np.divide(state, 2.0)
        state = np.add(state, 0.5)
        newState = state.reshape(1, self.inputSize)
        return newState