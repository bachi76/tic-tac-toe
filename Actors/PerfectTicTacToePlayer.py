import random
import numpy as np

'''
    This player follows hardcoded deterministic tic-tac-toe rules for decisive moves (2-in-a-row or free center),
    and random moves for others. To be used as trainers for ml players.
'''
class PerfectTicTacToePlayer:
    val = None

    def __init__(self):
        pass

    def move(self, game):

        b = game.board

        # mode = True: Check for wins; mode = False: check for defense moves
        for mode in [True, False]:

            r = self.checkLine(b[0, :], mode)
            if r > -1: return r

            r = self.checkLine(b[1, :], mode)
            if r > -1: return 3 + r

            r = self.checkLine(b[2, :], mode)
            if r > -1: return 6 + r

            r = self.checkLine(b[:, 0], mode)
            if r > -1: return r * 3

            r = self.checkLine(b[:, 1], mode)
            if r > -1: return 1 + r * 3

            r = self.checkLine(b[:, 2], mode)
            if r > -1: return 2 + r * 3

            r = self.checkLine(b.diagonal(), mode)
            if r > -1: return r * 3 + r

            r = self.checkLine(np.fliplr(b).diagonal(), mode)
            if r > -1: return r * 3 + (2 - r)


        # Center move
        if b[1, 1] == 0: return 4

        # or else a random move
        return random.choice(game.available_actions())


    # mode = True: Check for wins; mode = False: check for defense moves
    def checkLine(self, row, mode):
        row = list(row)
        if mode is True and row.count(0) == 1 and row.count(self.val) == 2:
            return row.index(0)

        if mode is False and row.count(0) == 1 and row.count(self.val * -1) == 2:
            return row.index(0)

        return -1

    def reward(self, game, reward):
        pass
