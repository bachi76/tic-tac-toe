from __future__ import print_function

import random

import tensorflow.contrib.learn as skflow
import numpy as np

"""
Example of a standard NN and supervised learning. Obviously lacking 'defensive training' (training on lost games)
"""

class DNNPlayer:

    def __init__(self):

        self.classifier = skflow.TensorFlowDNNClassifier(
            hidden_units=[30, 40, 30],
            steps=6000,
            n_classes=9)


    def move(self, game):

        available_moves = game.available_actions()

        # First moves can be random for now
        if len(game.history) < 1:
            return random.choice(available_moves)

        # Create inputs from the current and previous board state
        inputs = np.array(np.concatenate([
            game.board,
            game.history[-1]
        ]).flatten(), ndmin=2)

        pred = self.classifier.predict(inputs)

        # print(inputs)
        # print("Prediction: {}".format(pred))

        # Predicted field is not empty? -> random move
        if pred not in available_moves:
            return random.choice(available_moves)

        return pred[0]


    def reward(self, game, reward):
        # ugn?
        pass


    def train(self, history):

        X = np.zeros((len(history), 18))
        y = np.zeros((len(history), 1))
        i = 0

        for game in history:

            # Train only on wins of player A (+1 values)
            if game.state == game.WIN_P1:

                # Get the second last board states (that lead to the winning move)
                X[i] = np.concatenate([
                    game.history[-2],
                    game.history[-3]
                ]).flatten()

                y[i] = game.last_move

                # print(X[i])
                # print(".. and the winning move is: {}".format(y[i]))
                # print()

            # TODO: How can we train lost games with a DNN classifier?
            # if game.state == game.WIN_PMINUS1:

            i += 1

        self.classifier.fit(X, y)
