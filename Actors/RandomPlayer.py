import random
import numpy as np


class RandomPlayer:

    def __init__(self):
        random.seed()

    def move(self, game):

        # Pick a free field at random :)
        return random.choice(game.available_actions())

    def reward(self, game, reward):
        # ugn?
        pass
