import hashlib
import random


class QLearningPlayer:
    def __init__(self):

        self.RANDOM_QUOTE_START = 1.0000
        self.RANDOM_QUOTE_STOP = 0.001
        self.RANDOM_QUOTE_DECREASE = 0.00001

        self.BACKPROP_STEPS = 5

        self.val = None
        self.memory = {}
        self.stateHistory = []
        self.randomQuote = self.RANDOM_QUOTE_START

        random.seed()

    def move(self, game):

        # For all possible moves, try to find a q-value entry in our memory
        # Select the best move. If we have no entry, choose a random move.

        bestMove = random.choice(game.available_actions())
        bestQValue = 0
        
        if random.random() > self.randomQuote:
            
            for action in game.available_actions():
                examinedState = game.board.copy()
                examinedState.flat[action] = self.val
                examinedStateKey = self.hash(examinedState)

                if examinedStateKey in self.memory and self.memory[examinedStateKey] > bestQValue:
                    bestQValue = self.memory[examinedStateKey]
                    bestMove = action

        # Keep our own history (when the environment calls reward(), the state could have changed from the other player's move)
        # newState = game.board.copy()
        # newState[bestMove] = self.val
        # self.stateHistory.append(newState)

        # print(game.board)
        # print("=> best move: {} with q-value {}".format(bestMove, bestQValue))

        self.randomQuote = max(self.randomQuote - self.RANDOM_QUOTE_DECREASE, self.RANDOM_QUOTE_STOP)
        # print("Random quote: {}".format(self.randomQuote))

        return bestMove

    # Called by the game engine if the last action leads to a reward
    def reward(self, game, reward):

        '''
        TODO:   For Deep Reinforced Learning with a DQL, add an ANN here to deal with the reward
                http://arxiv.org/pdf/1312.5602.pdf)
                https://www.nervanasys.com/demystifying-deep-reinforcement-learning/
                (or policy gradients instead of DQL: http://karpathy.github.io/2016/05/31/rl/)

        '''

        # Propagate the discounted reward backward in the memory
        if game.last_player == self.val:
            idx = -1
        else:
            idx = -2

        for i in range(0, self.BACKPROP_STEPS):

            try:
                state = game.history[idx]
                self.updateQValue(state, reward)
                reward /= 2
                idx -= 2
            except IndexError:
                pass


    def updateQValue(self, state, value):
        key = self.hash(state)
        if key in self.memory:
            self.memory[key] += value
        else:
            self.memory[key] = value


    def hash(self, state):
        # Could probably be optimized
        # maybe hash(str(state))?
        return hashlib.sha1(state).hexdigest()
