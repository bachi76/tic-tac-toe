import random
import numpy as np


class HumanPlayer:

    def __init__(self, numBlockStyle=True):
        self.numBlockStyle = numBlockStyle

    def move(self, game):

        if game.last_move != -1:
            print("Opponent plays {}".format(self.invertNumBlock(game.last_move + 1)))

        print(game.board)
        print("Available moves: {}".format([self.invertNumBlock(x) for x in np.add(game.available_actions(), 1)]))

        move = -1
        while move not in game.available_actions():
            move = self.invertNumBlock(input("Your choice? ")) - 1

        return move


    def reward(self, game, reward):

        print(game.board)

        if reward > 0:
            print("Congrats, you won!")
        elif reward < 0:
            print("Skynet just became self-aware!")
        else:
            print("It's a draw.")
        print ("___________________")
        print("")


    def invertNumBlock(self, key):
        if not self.numBlockStyle:
            return key

        if key in [1, 2, 3]:
            key += 6
        elif key in [7, 8, 9]:
            key -= 6

        return key