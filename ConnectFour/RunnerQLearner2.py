from __future__ import print_function

import numpy as np
from BoardGamesSimulator.Actors.HumanPlayer import HumanPlayer
from BoardGamesSimulator.Actors.QLearningPlayer import QLearningPlayer
from BoardGamesSimulator.Actors.RandomPlayer import RandomPlayer

from BoardGamesSimulator import Environment
from BoardGamesSimulator.ConnectFour.ConnectFourGame import ConnectFourGame
from BoardGamesSimulator.TicTacToe.TicTacToeGame import TicTacToeGame

"""
The starting point for experiments. Define the players and run simulations.
"""


''' QLearningPlayer '''
1

print("Starting QLearningPlayer training...")
playerQLearn1 = QLearningPlayer()
playerQLearn2 = QLearningPlayer()

for i in range(40):
    print("Batch {}".format(i))
    # Start qLearner as Player B, since it'll play against the human Player A later
    history = Environment.run(playerQLearn1, playerQLearn2, ConnectFourGame, 3000)[0]

    # Show an example of a lost game.
    for game in [x for x in history if x.state == x.WIN_PMINUS1][0:1]:
        print(np.array(game.history))


print("")
print("Test against random player")

for i in range(40):
    print("Batch {}".format(i))
    history = Environment.run(RandomPlayer(), playerQLearn1, ConnectFourGame, 1000)[0]

    # Show an example of a lost game.
    for game in [x for x in history if x.state == x.WIN_PMINUS1][0:1]:
        print(np.array(game.history))


print("")



''' Now you play '''
# Your turn to play...
print("")
print("---- Your turn, human! (5 games) ----")
Environment.run(HumanPlayer(numBlockStyle=False), playerQLearn2, ConnectFourGame, 5)