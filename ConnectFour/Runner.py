from BoardGamesSimulator import Environment
from BoardGamesSimulator.Actors.HumanPlayer import HumanPlayer
from BoardGamesSimulator.Actors.RandomPlayer import RandomPlayer
from BoardGamesSimulator.ConnectFour.ConnectFourGame import ConnectFourGame
import numpy as np

history = Environment.run(HumanPlayer(numBlockStyle=False), RandomPlayer(), ConnectFourGame, 5)[0]

