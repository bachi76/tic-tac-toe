from __future__ import print_function
import numpy as np
from skimage.util import view_as_windows

class ConnectFourGame:

    IN_PROGRESS = 0
    DRAW = 1
    WIN_P1 = 2
    WIN_PMINUS1 = 3


    def __init__(self):

        self.state = self.IN_PROGRESS
        self.last_player = 0
        self.last_move = -1

        self.boardSizeX = 6
        self.boardSizeY = 6
        self.connect = 4

        # Store the game state and history (without compressing it)
        self.board = np.zeros((self.boardSizeY, self.boardSizeX))
        self.history = []


    def move(self, player, pos):

        if player not in [-1, 1]:
            raise ValueError("Player must be 1 or -1!")

        if self.state != self.IN_PROGRESS:
            raise OverflowError("Game already finished!")

        if player == self.last_player:
            raise ValueError("Not your turn!")

        # if self.board.flat[pos] == 0:
        selectedCol = self.board[:, pos]
        if 0 in selectedCol:
            posFinal = np.amax(np.where(selectedCol == 0)[0])

            selectedCol[posFinal] = player
            # Make the move and store board state in history
            # self.board.flat[pos] = player

            self.history.append(self.board.copy())

            self.last_player = player
            self.last_move = pos
            self.update_state()

        else:
            raise ValueError("Column already full!")


    def update_state(self):

        # Split the board in sliding windows with step size 1 and check each for wins
        windows = view_as_windows(self.board, window_shape=(self.connect, self.connect)).reshape((-1, 4, 4))
        for window in windows:

            # Do some np matrix magic to get an array of all sums of all board lines
            checks = np.concatenate([
                window.sum(0),
                window.sum(1),
                [window.trace()],
                [np.fliplr(window).trace()]
            ])

            # If we have {self.connect} or -{self.connect} as sum of any line, it's a win
            if self.connect in checks:
                self.state = self.WIN_P1
                return

            elif -self.connect in checks:
                self.state = self.WIN_PMINUS1
                return


        # End if all fields are occupied
        if 0 in self.board.flat:
            self.state = self.IN_PROGRESS

        else:
            self.state = self.DRAW


    # Get all actions that are available in the current state
    def available_actions(self):

        if self.state == self.IN_PROGRESS:
            # return np.where(self.board.flatten() == 0)[0]
            available = []
            for i in range(self.boardSizeX):
                if 0 in self.board[:, i]:
                    available.append(i)

            return available

        else:
            return np.array()


