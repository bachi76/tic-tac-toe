from __future__ import print_function

import random

from BoardGamesSimulator.TicTacToe import TicTacToeGame

REWARD_WIN = 1.0
REWARD_LOSS = -1.0
REWARD_DRAW = 0.0

def run(playerA, playerB, gameClass, sims):

    # The history of all games (each containing the game state history)
    history = []

    winsA = 0.0
    winsB = 0.0
    draws = 0.0

    # Assign values to represent players on the board
    playerA.val = 1
    playerB.val = -1

    for i in range(0, sims):
        game = gameClass()
        nextIsA = random.choice([True, False])

        while game.state == game.IN_PROGRESS:
            player = playerA if nextIsA else playerB
            nextIsA = not nextIsA
            move = player.move(game)
            game.move(player.val, move)

        history.append(game)

        if game.state == game.WIN_P1:
            winsA += 1
            playerA.reward(game, REWARD_WIN)
            playerB.reward(game, REWARD_LOSS)

        elif game.state == game.WIN_PMINUS1:
            winsB += 1
            playerA.reward(game, REWARD_LOSS)
            playerB.reward(game, REWARD_WIN)

        else:
            draws += 1
            playerA.reward(game, REWARD_DRAW)
            playerB.reward(game, REWARD_DRAW)

    print("Sim results:")
    print("Player A: {} ({} %)".format(winsA, winsA / sims * 100))
    print("Player B: {} ({} %)".format(winsB, winsB / sims * 100))
    print("Draws: {} ({} %)".format(draws, draws / sims * 100))

    return history, winsA, winsB, draws



