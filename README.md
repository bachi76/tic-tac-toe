# Tic-Tac-Toe Simulator #

This is a tic-tac-toe simulator, used for experiments with AI agents. 

Currently, there are 3 players:

* DNNPlayer.py: Half-working AI included, which uses an ANN based on TensorFlow and Scikit Flow (now TensorFlow Learn). Does only train on the last winning moves - so clearly not so helpful
* QLearningPlayer.py: Simple Q-learning based agent. 
* HumanPlayer: That's you.

Both ML agents know nothing about the game rules, and uses generated random player games as learning input. 

Typical winning stats:
```
Player Random: 103.0 (3.4 %)
Player QLearner: 2767.0 (92.2 %)
Draws: 130.0 (4.3 %)
```

**Starting point for experiments: Runner.py and RunnerQLearner.py**

Suggested next step: Using QLearningPlayer as starting point, create a DQLPlayer using Deep Q-Learning (combine Q-Learning and an ANN to 'store' reward information)

## Requirements: ##
Scikit Learn, TensorFlow 0.8

There's also a repo containing a docker-based Jupyter environment including all required packages: https://bitbucket.org/bachi76/docker-ipython-tensorflow